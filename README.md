# ANSI Escape Code support library for Dart

Use [ANSI Escape Codes](https://en.wikipedia.org/wiki/ANSI_escape_code) to apply some formatting to your text.

Can easily be globally disabled, by setting `AnsiColorizer.enableColorizers` to `false`.

# Example

```dart
import "package:ansi_colorizer/ansi_colorizer.dart";

void main() {
  // Create an AnsiColorizer object with your specific formatting options.
  const colorizer = AnsiColorizer(
    foreground: Ansi24BitColor.fromRGB(210, 200, 0),
    modifiers: {
      AnsiModifier.bold,
      AnsiModifier.italic,
    },
  );

  // Apply the formatting to the string representation of an object
  // by calling using the colorizer variable like a function.
  //
  // Alternatively you can also use:
  // final result = colorizer.colorize("Hello World");
  final result = colorizer("Hello World");

  // Prints "Hello World", but the text color is yellow and the
  // text is bold and cursive.
  print(result);
}
```

# Colors

This library supports [8 bit](https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit) and [24 bit](https://en.wikipedia.org/wiki/ANSI_escape_code#24-bit) colors.

## 3 Bit

This table represents all available colors.

![table](https://gitlab.com/flutterpackages/ansi_colorizer/-/raw/master/assets/3bit_colors.png)

```dart
...
// The standard color for cyan (see the color table).
final color = Ansi3BitColor(6);
...
```

All 3 bit colors can also be accessed with the `Ansi3BitColors` class.

```dart
...
final color = Ansi3BitColors.blue;
...
```

## 8 Bit

This table represents all available colors.

![table](https://gitlab.com/flutterpackages/ansi_colorizer/-/raw/master/assets/8bit_colors.png)

```dart
...
// The standard color for red (see the color table).
final color = Ansi8BitColor(1);
...
```

## 24 Bit

Create a color from a red, green and blue component.

```dart
...
final red = Ansi24BitColor(0xFF0000);
final green = Ansi24BitColor.fromRGB(0, 0xFF, 0);
...
```

# Modifiers

You can also apply a modifier from the `AnsiModifier` enum to make your text bold for example.

