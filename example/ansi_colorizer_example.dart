import "package:ansi_colorizer/ansi_colorizer.dart";

void main() {
  // Create an AnsiColorizer object with your specific formatting options.
  const colorizer = AnsiColorizer(
    foreground: Ansi24BitColor.fromRGB(210, 200, 0),
    modifiers: {
      AnsiModifier.bold,
      AnsiModifier.italic,
    },
  );

  // Apply the formatting to the string representation of an object
  // by calling using the colorizer variable like a function.
  //
  // Alternatively you can also use:
  // final result = colorizer.colorize("Hello World");
  final result = colorizer("Hello World");

  // Prints "Hello World", but the text color is yellow and the
  // text is bold and cursive.
  print(result);
}
