# 1.1.1

  * Added class `Ansi3BitColors`

  * Added method `AnsiColorizer.copyWith`

  * Added section to README.md for `Ansi3BitColor`

# 1.1.0

  * Added class `Ansi3BitColor`

  * Renamed `AnsiColorizer.alwaysUse8BitColors` to `AnsiColorizer.convert24BitColors`

  * Added more tests

# 1.0.0

  * Initial version.
