import "dart:io";

bool get supportsAnsiEscapes => stdout.supportsAnsiEscapes;
