part of "../ansi_colorizer.dart";

/// This class represents an ANSI Color.
sealed class AnsiColor {
  const AnsiColor();
}

/// This class represents a 24-bit ANSI Color, which has a red, green and
/// blue part.
final class Ansi24BitColor extends AnsiColor {
  /// Construct a color from the lower 24 bits of an int.
  ///
  /// The bits are interpreted as follows:
  ///
  /// * Bits 16-23 are the red value
  /// * Bits 8-15 are the green value
  /// * Bits 0-7 are the blue value
  ///
  /// In other words, if RR is the red value in hex, GG the green value in hex
  /// and BB the blue value in hex, a color can be expressed as
  /// `const Ansi24BitColor(0xRRGGBB)`.
  ///
  /// For example, to get orange, you would use `const Ansi24BitColor(0xFF9000)`
  /// (FF for the red, 90 for the green, and 00 for the blue).
  const Ansi24BitColor(int color) : value = color & 0xFFFFFF;

  const Ansi24BitColor.fromRGB(int red, int green, int blue)
      : value = (blue & 0xFF) | ((green & 0xFF) << 8) | ((red & 0xFF) << 16);

  /// A 24 bit value representing this color.
  final int value;

  @override
  int get hashCode => value.hashCode;

  /// The red channel of this color in an 8 bit value.
  int get red => (value & 0xFF0000) >> 16;

  /// The green channel of this color in an 8 bit value.
  int get green => (value & 0x00FF00) >> 8;

  /// The blue channel of this color in an 8 bit value.
  int get blue => (value & 0x0000FF) >> 0;

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Ansi24BitColor && other.value == value;
  }

  /// Convert this [Ansi24BitColor] to one of the 216 colors from
  /// the 6x6x6 cube.
  ///
  /// See https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit
  int _to8Bit() {
    // 6 × 6 × 6 cube (216 colors): 16 + 36 × r + 6 × g + b (0 ≤ r, g, b ≤ 5)

    final r = (5 * red / 0xFF).round();
    final g = (5 * green / 0xFF).round();
    final b = (5 * blue / 0xFF).round();

    return 16 + 36 * r + 6 * g + b;
  }

  @override
  String toString() {
    return "Ansi24BitColor(0x${value.toRadixString(16).padLeft(8, '0')})";
  }

  /// Returns a new color that matches this color with the red channel
  /// replaced with [r] (which ranges from 0 to 255).
  ///
  /// Out of range values will have unexpected effects.
  Ansi24BitColor withRed(int r) => Ansi24BitColor.fromRGB(r, green, blue);

  /// Returns a new color that matches this color with the green channel
  /// replaced with [g] (which ranges from 0 to 255).
  ///
  /// Out of range values will have unexpected effects.
  Ansi24BitColor withGreen(int g) => Ansi24BitColor.fromRGB(red, g, blue);

  /// Returns a new color that matches this color with the blue channel
  /// replaced with [b] (which ranges from 0 to 255).
  ///
  /// Out of range values will have unexpected effects.
  Ansi24BitColor withBlue(int b) => Ansi24BitColor.fromRGB(red, green, b);
}

/// This class represents an ANSI Color from the [6x6x6 ANSI color cube](https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit).
///
/// This table represents all available colors.
///
/// ![table](https://gitlab.com/flutterpackages/ansi_colorizer/-/raw/master/assets/8bit_colors.png)
final class Ansi8BitColor extends AnsiColor {
  /// Construct a color from the lower 8 bits of an [int].
  ///
  /// The bits are interpreted as the index of a 8-bit ANSI Color
  /// from the 6x6x6 ANSI Color cube.
  ///
  /// For example, to get standard red, you would use `const Ansi8BitColor(1)`.
  ///
  /// See https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit
  const Ansi8BitColor(int value) : value = value & 0xFF;

  /// A 8 bit value representing this color.
  ///
  /// This is effectively the index of this [Ansi8BitColor] in the color cube.
  /// See https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit
  final int value;

  @override
  int get hashCode => value.hashCode;

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Ansi8BitColor && other.value == value;
  }

  @override
  String toString() {
    return "Ansi8BitColor($value)";
  }
}

/// This class represents an ANSI Color from the [original VGA specification](https://en.wikipedia.org/wiki/ANSI_escape_code#3-bit_and_4-bit).
///
/// This table represents all available VGA colors. Keep in mind that
/// [Ansi3BitColor] only wants the index of the color ranging from `0 to 7`,
/// both inclusive.
///
/// ![table](https://gitlab.com/flutterpackages/ansi_colorizer/-/raw/master/assets/3bit_colors.png)
///
/// The [Ansi3BitColor] is used as a foreground color if it is the value of
/// [AnsiColorizer.foreground] and as a background color if it is the value
/// of [AnsiColorizer.background]. Also whether the color is a bright color or
/// a normal one depends on the value of [Ansi3BitColor.isBright].
final class Ansi3BitColor extends AnsiColor {
  /// Construct a color from the lower 3 bits of an [int].
  ///
  /// The bits are interpreted as the index of a 3-bit ANSI Color
  /// from original ANSI specification.
  ///
  /// For example, to get white, you would use `const Ansi3BitColor(7)`.
  ///
  /// See https://en.wikipedia.org/wiki/ANSI_escape_code#3-bit_and_4-bit
  const Ansi3BitColor(
    int value, {
    this.isBright = false,
  }) : value = value & 0x07;

  /// A 3 bit value representing this color.
  final int value;

  final bool isBright;

  @override
  int get hashCode => Object.hash(value, isBright);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Ansi3BitColor &&
        other.value == value &&
        other.isBright == isBright;
  }

  int _getSGRParameter(bool isForeground) {
    return value + (isForeground ? 30 : 40) + (isBright ? 60 : 0);
  }

  @override
  String toString() {
    return "Ansi3BitColor($value, isBright: $isBright)";
  }
}
