part of "../ansi_colorizer.dart";

sealed class _SgrParams {
  static const escape = "\x1B[";

  static const end = "0";

  static const set = "m";

  static const seper = ";";

  static const foregroundColor = "38";

  static const backgroundColor = "48";

  static const set8BitColor = "5";

  static const setRgbColor = "2";
}
