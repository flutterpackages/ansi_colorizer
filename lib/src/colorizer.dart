part of "../ansi_colorizer.dart";

/// A class that can be used to apply [ANSI Escape Codes](https://en.wikipedia.org/wiki/ANSI_escape_code)
/// to some text.
///
/// ### Example
///
/// ```dart
/// import "package:ansi_colorizer/ansi_colorizer.dart";
///
/// void main() {
///   // Create an AnsiColorizer object with your specific formatting options.
///   const colorizer = AnsiColorizer(
///     foreground: Ansi24BitColor.fromRGB(210, 200, 0),
///     modifiers: {
///       AnsiModifier.bold,
///       AnsiModifier.italic,
///     },
///   );
///
///   // Apply the formatting to the string representation of an object
///   // by calling using the colorizer variable like a function.
///   //
///   // Alternatively you can also use:
///   // final result = colorizer.colorize("Hello World");
///   final result = colorizer("Hello World");
///
///   // Prints "Hello World", but the text color is yellow and the
///   // text is bold and cursive.
///   print(result);
/// }
///
/// ```
base class AnsiColorizer {
  const AnsiColorizer({
    this.foreground,
    this.background,
    Set<AnsiModifier> modifiers = const {},
    this.convert24BitColors = false,
  }) : _modifiers = modifiers;

  /// The foreground color of the text.
  final AnsiColor? foreground;

  /// The background color of the text.
  final AnsiColor? background;

  /// Modifiers that will be applied to the text.
  Set<AnsiModifier> get modifiers => UnmodifiableSetView(_modifiers);
  final Set<AnsiModifier> _modifiers;

  /// True if all [Ansi24BitColor]s should be converted to
  /// an [Ansi8BitColor].
  ///
  /// Defaults to `false`.
  final bool convert24BitColors;

  /// True if the current platform supports [ANSI Escape Codes](https://en.wikipedia.org/wiki/ANSI_escape_code)
  static bool get isSupported => supportsAnsiEscapes;

  /// True if all [AnsiColorizer]s should be enabled.
  ///
  /// Defaults to the value of [isSupported].
  ///
  /// If this value is set to false, then a call to [AnsiColorizer.colorize]
  /// returns the string representation of the provided object without
  /// any ANSI Escape Codes for every [AnsiColorizer] instance.
  static bool enableColorizers = isSupported;

  /// Colorize the result of calling `object.toString()`.
  @nonVirtual
  String call(Object? object) => colorize(object);

  /// Colorize the result of calling `object.toString()`.
  @nonVirtual
  String colorize(Object? object) {
    if (!enableColorizers) return object.toString();

    final buf = StringBuffer();

    buf.write(_SgrParams.escape);

    for (final item in _modifiers) {
      buf.write(item._code);
      buf.write(_SgrParams.seper);
    }

    if (null != foreground) {
      _writeColor(buf, foreground!, true);
    }

    if (null != background) {
      _writeColor(buf, background!, false);
    }

    buf.write(_SgrParams.set);

    buf.write(object);

    buf.write(_SgrParams.escape);
    buf.write(_SgrParams.end);
    buf.write(_SgrParams.set);

    return buf.toString();
  }

  void _writeAs8BitColor(StringBuffer buf, int color) {
    buf.write(_SgrParams.set8BitColor);
    buf.write(_SgrParams.seper);
    buf.write(color);
  }

  void _writeColor(StringBuffer buf, AnsiColor color, bool isForeground) {
    if (color is! Ansi3BitColor) {
      buf.write(
        isForeground ? _SgrParams.foregroundColor : _SgrParams.backgroundColor,
      );
      buf.write(_SgrParams.seper);
    }

    switch (color) {
      case Ansi24BitColor(red: final r, green: final g, blue: final b):
        if (convert24BitColors) {
          _writeAs8BitColor(buf, color._to8Bit());
        } else {
          buf.write(_SgrParams.setRgbColor);
          buf.write(_SgrParams.seper);
          buf.write(r);
          buf.write(_SgrParams.seper);
          buf.write(g);
          buf.write(_SgrParams.seper);
          buf.write("$b");
        }
        break;

      case Ansi8BitColor():
        _writeAs8BitColor(buf, color.value);
        break;

      case Ansi3BitColor():
        buf.write(color._getSGRParameter(isForeground));
        break;
    }
  }

  AnsiColorizer copyWith({
    AnsiColor? foreground,
    AnsiColor? background,
    Set<AnsiModifier>? modifiers,
    bool? convert24BitColors,
  }) {
    return AnsiColorizer(
      foreground: foreground ?? this.foreground,
      background: background ?? this.background,
      modifiers: modifiers ?? this.modifiers.toSet(),
      convert24BitColors: convert24BitColors ?? this.convert24BitColors,
    );
  }
}
