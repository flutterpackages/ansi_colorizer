part of "../ansi_colorizer.dart";

enum AnsiModifier implements Comparable<AnsiModifier> {
  /// Increases the intensity of the text.
  bold,

  /// Decreases the intensity of the text.
  faint,

  /// Makes the text cursive.
  italic,

  underline;

  int get _code {
    // See https://en.wikipedia.org/wiki/ANSI_escape_code#CSI_(Control_Sequence_Introducer)_sequences
    return switch (this) {
      bold => 1,
      faint => 2,
      italic => 3,
      underline => 4,
    };
  }

  @override
  int compareTo(AnsiModifier other) => other.index - index;
}
