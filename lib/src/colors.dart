part of "../ansi_colorizer.dart";

sealed class Ansi3BitColors {
  static const black = Ansi3BitColor(0);

  static const red = Ansi3BitColor(1);

  static const green = Ansi3BitColor(2);

  static const yellow = Ansi3BitColor(3);

  static const blue = Ansi3BitColor(4);

  static const magenta = Ansi3BitColor(5);

  static const cyan = Ansi3BitColor(6);

  static const white = Ansi3BitColor(7);

  static const brightBlack = Ansi3BitColor(0, isBright: true);
  static const gray = brightBlack;

  static const brightRed = Ansi3BitColor(1, isBright: true);

  static const brightGreen = Ansi3BitColor(2, isBright: true);

  static const brightYellow = Ansi3BitColor(3, isBright: true);

  static const brightBlue = Ansi3BitColor(4, isBright: true);

  static const brightMagenta = Ansi3BitColor(5, isBright: true);

  static const brightCyan = Ansi3BitColor(6, isBright: true);

  static const brightWhite = Ansi3BitColor(7, isBright: true);
}
