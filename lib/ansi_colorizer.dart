library ansi_colorizer;

import "dart:collection";

import "package:meta/meta.dart";

import "src/support/support.dart"
    if (dart.library.io) "src/support/support_io.dart"
    if (dart.library.html) "src/support/support_web.dart";

part "src/sgr_params.dart";
part "src/color.dart";
part "src/colors.dart";
part "src/colorizer.dart";
part "src/modifier.dart";
