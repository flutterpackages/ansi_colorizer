import "package:ansi_colorizer/ansi_colorizer.dart";
import "package:test/expect.dart";
import "package:test/scaffolding.dart";

void main() {
  const text = "FooBar";

  group("Ansi24BitColor", () {
    test("foreground + modifiers", () {
      const colorizer = AnsiColorizer(
        foreground: Ansi24BitColor.fromRGB(210, 200, 0),
        modifiers: {
          AnsiModifier.bold,
        },
      );

      AnsiColorizer.enableColorizers = true;
      expect(colorizer(text), "\x1B[1;38;2;210;200;0m$text\x1B[0m");

      AnsiColorizer.enableColorizers = false;
      expect(colorizer(text), text);
    });
  });

  group("Ansi8BitColor", () {
    test("foreground + modifiers", () {
      const colorizer = AnsiColorizer(
        foreground: Ansi8BitColor(3),
        modifiers: {
          AnsiModifier.faint,
        },
      );

      AnsiColorizer.enableColorizers = true;
      expect(colorizer(text), "\x1B[2;38;5;3m$text\x1B[0m");

      AnsiColorizer.enableColorizers = false;
      expect(colorizer(text), text);
    });

    test("foreground", () {
      const colorizer = AnsiColorizer(
        foreground: Ansi8BitColor(1),
      );

      AnsiColorizer.enableColorizers = true;
      expect(colorizer(text), "\x1B[38;5;1m$text\x1B[0m");

      AnsiColorizer.enableColorizers = false;
      expect(colorizer(text), text);
    });
  });

  group("Ansi3BitColor", () {
    test("foreground + modifiers", () {
      const colorizer = AnsiColorizer(
        foreground: Ansi3BitColor(4),
        modifiers: {
          AnsiModifier.underline,
        },
      );

      AnsiColorizer.enableColorizers = true;
      expect(colorizer(text), "\x1B[4;34m$text\x1B[0m");

      AnsiColorizer.enableColorizers = false;
      expect(colorizer(text), text);
    });

    test("foreground", () {
      const colorizer = AnsiColorizer(
        foreground: Ansi3BitColor(7),
      );

      AnsiColorizer.enableColorizers = true;
      expect(colorizer(text), "\x1B[37m$text\x1B[0m");

      AnsiColorizer.enableColorizers = false;
      expect(colorizer(text), text);
    });

    test("bright foreground", () {
      const colorizer = AnsiColorizer(
        foreground: Ansi3BitColor(5, isBright: true),
      );

      AnsiColorizer.enableColorizers = true;
      expect(colorizer(text), "\x1B[95m$text\x1B[0m");

      AnsiColorizer.enableColorizers = false;
      expect(colorizer(text), text);
    });

    test("background", () {
      const colorizer = AnsiColorizer(
        background: Ansi3BitColor(0),
      );

      AnsiColorizer.enableColorizers = true;
      expect(colorizer(text), "\x1B[40m$text\x1B[0m");

      AnsiColorizer.enableColorizers = false;
      expect(colorizer(text), text);
    });

    test("bright background", () {
      const colorizer = AnsiColorizer(
        background: Ansi3BitColor(3, isBright: true),
      );

      AnsiColorizer.enableColorizers = true;
      expect(colorizer(text), "\x1B[103m$text\x1B[0m");

      AnsiColorizer.enableColorizers = false;
      expect(colorizer(text), text);
    });
  });
}
